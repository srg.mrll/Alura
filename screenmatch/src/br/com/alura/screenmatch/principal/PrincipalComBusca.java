package br.com.alura.screenmatch.principal;


import br.com.alura.screenmatch.files.Files;
import br.com.alura.screenmatch.shearch.SearchTitle;
import br.com.alura.screenmatch.modelos.Titulo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrincipalComBusca {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("*******************************************");
        System.out.println("BUSCA API OMDB");
        System.out.println("*******************************************");

        Scanner leitura = new Scanner(System.in);
        String busca = "";
        List<Titulo> titulos = new ArrayList<>();

        while(!busca.equals("sair")) {

            System.out.println("Digite um filme para busca: ");
            busca = leitura.nextLine();

            if (busca.equals("sair")){
                break;
            }

            SearchTitle srTitulo = new SearchTitle();
            titulos.add(srTitulo.SearchTitle(busca));
        }

        Files file = new Files("filmes.txt",titulos);
        file.escreveTitulo();

        System.out.println("O programa finalizou !");

    }
}
