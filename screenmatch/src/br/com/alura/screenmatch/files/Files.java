package br.com.alura.screenmatch.files;

import br.com.alura.screenmatch.modelos.Titulo;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Files {
    private String nome;
    private List<Titulo> titulo;

    public Files(String nome, List<Titulo> titulo) {
        this.nome = nome;
        this.titulo = titulo;
    }

    public void escreveTitulo() throws IOException {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
        try {
            File file = new File("E:\\Users\\murillosmsd\\Documents\\Alura\\screenmatch\\src\\br\\com\\alura\\screenmatch\\files\\fileOut\\titulos.json");
            FileWriter filew = new FileWriter(file);
            filew.write(gson.toJson(titulo));
            filew.close();
        } catch (NumberFormatException e) {
            System.out.println("Aconteceu um erro : " + e.getMessage());
        }
    }
}
