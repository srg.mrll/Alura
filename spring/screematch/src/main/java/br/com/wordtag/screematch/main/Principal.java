package br.com.wordtag.screematch.main;

import br.com.wordtag.screematch.model.DadosEpisodio;
import br.com.wordtag.screematch.model.DadosSerie;
import br.com.wordtag.screematch.model.DadosTemporada;
import br.com.wordtag.screematch.model.Episodio;
import br.com.wordtag.screematch.service.ConsumoAPI;
import br.com.wordtag.screematch.service.ConverteDados;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Principal {
    private static final String omdb = "https://www.omdbapi.com/?t=";
    private static final String apiKey = "&apikey=2bda8e71";

    private static ConverteDados conversor = new ConverteDados();
    private static ConsumoAPI consumoAPI = new ConsumoAPI();
    private Scanner sc = new Scanner(System.in);
    public String menu(){
        System.out.println("###############################################");
        System.out.println("Informe o nome da série.");
        System.out.println("###############################################");
        var serie = sc.nextLine();
        var url = omdb + serie.replace(" ", "+") + apiKey;

        return consumoAPI.obterDados(url);

    }
    public static DadosSerie dadosSerie(String json){
        DadosSerie dadosSerie = conversor.obterDados(json,DadosSerie.class);
        System.out.println(dadosSerie);
        System.out.println("*****************************************************");
        return dadosSerie;
    }
    public static void dadosTemporada(String json, Integer totalTemporadas, String serie){
        List<DadosTemporada> dadosTemporada = new ArrayList<>();
        try{
            for (int i =1; i <= totalTemporadas; i++){
                var endereco = omdb + serie.replace(" ","+") + "&season=" + i + apiKey;
                json = consumoAPI.obterDados(endereco);
                dadosTemporada.add(conversor.obterDados(json, DadosTemporada.class));
            }
        }catch (NullPointerException e){
            e.getMessage();
        }


        //dadosTemporada.forEach(t-> t.episodios().forEach(e-> System.out.println(e.titulo())));

        //dadosTemporada.forEach(System.out::println);

        List<DadosEpisodio> dadosEpisodios = dadosTemporada.stream()
                .flatMap(t -> t.episodios().stream()).toList();


//        dadosEpisodios.stream().filter(e -> !e.avaliacao()
//                .equalsIgnoreCase("N/A"))
//                .sorted(Comparator.comparing(DadosEpisodio::avaliacao)
//                        .reversed()).limit(5)
//                .forEach(System.out::println);

        List<Episodio> episodios = dadosTemporada.stream()
                .flatMap(t -> t.episodios().stream()
                        .sorted(Comparator.comparing(DadosEpisodio::avaliacao)
                                .reversed())
                        .limit(5)
                        .map(d -> new Episodio(t.temporada(),d)))
                        .collect(Collectors.toList());

        episodios.forEach(System.out::println);
    }
}
