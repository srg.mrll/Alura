package br.com.wordtag.screematch;

import br.com.wordtag.screematch.main.Principal;
import br.com.wordtag.screematch.model.DadosSerie;
import br.com.wordtag.screematch.model.DadosTemporada;
import br.com.wordtag.screematch.model.Episodio;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class ScreematchApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ScreematchApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Principal principal = new Principal();

		var json = principal.menu();

		DadosSerie dadosSerie = principal.dadosSerie(json);

		principal.dadosTemporada(json, dadosSerie.totalTemporadas(), dadosSerie.titulo());

	}

}
