package br.com.wordtag.screematch.model;

import com.fasterxml.jackson.annotation.JsonAlias;

public class Episodio {


    private String titulo;
    private Integer temporada;
    private Double avaliacao;

    public Episodio(Integer temporada, DadosEpisodio dadosEpisodio) {
        this.titulo = dadosEpisodio.titulo();
        this.temporada = temporada;
        try {
            this.avaliacao = Double.valueOf(dadosEpisodio.avaliacao());
        }catch (NumberFormatException ex){
            this.avaliacao = 0.0;
        }
    }

    @Override
    public String toString() {
        return "Temporada=" + temporada +
                ", Titulo='" + titulo + '\'' +
                ", Avaliacao=" + avaliacao;
    }
}
