package br.com.wordtag.screematch.service;

public interface IConverteDados {
   <T> T obterDados(String json,Class<T> classe);
}
