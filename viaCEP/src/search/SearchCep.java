package search;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SearchCep {
    public SearchCep(String busca) {
        try{
            //String apiKey = "2bda8e71";
            String endereco = "https://viacep.com.br/ws/" + busca + "/json/";

            endereco = endereco.replace(" ","");
            endereco = endereco.replace("-","");

            HttpClient client = HttpClient.newHttpClient();

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(endereco))
                    .build();

            HttpResponse<String> response = client
                    .send(request, HttpResponse.BodyHandlers.ofString());

            String json = response.body();

            //Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();


            System.out.println(json);

        }catch (NumberFormatException e){
            System.out.println("Aconteceu um erro: " + e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println("Erro de arguento na busca, favor verificar o endereço digitado: "
                    + e.getMessage());
        }catch (Exception e){
            System.out.println("Aconteceu um erro : " + e.getMessage());
        }
    }
}
